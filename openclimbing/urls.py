from django.conf.urls import patterns, include, url
from django.contrib import admin

from openclimbing import views

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'openclimbing.views.index', name='index'),
    url(r'^status/$', 'openclimbing.views.status', name='status'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^auth/', include([
        url(r'^login/$', views.LoginView.as_view(), name='auth_login'),
        url(r'^logout/$', 'openclimbing.views.auth_logout', name='auth_logout'),
        url(r'^register/$', views.RegisterView.as_view(), name='auth_register'),
        #url(r'^auth/login/$', views.LoginView.as_view(), name='login'),
    ])),

    # Django's admin site.
    url(r'^dj-admin/', include(admin.site.urls)),
    # TODOs
    url(r'^todo/$', views.todo, name='todo'),
    url(r'^robots.txt$', views.robots_txt, name='robots_txt'),
)