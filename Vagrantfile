# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 5000, host: 8000

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    # vb.gui = true

    # Customize the amount of memory on the VM:
    vb.memory = "1024"
    vb.cpus = 2
  end

  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update
    #sudo apt-get upgrade -y

    sudo apt-get install -y git curl libpcre3 libpcre3-dev libssl-dev
    sudo apt-get install -y python-pip python-dev postgresql postgresql-client postgresql-server-dev-all
    sudo apt-get install -y redis-server git curl libpcre3 libpcre3-dev libssl-dev

    sudo pip install -r /vagrant/requirements.txt

    # Database setup
    pg_file=$(find /etc/postgresql -name pg_hba.conf -print)
    pg_conf="host    all             all             127.0.0.1/32            trust"
    sudo grep -q -F "$pg_conf" $pg_file || echo "$pg_conf" >> $pg_file

    pg_role="CREATE ROLE openclimbing PASSWORD 'climb' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;"
    pg_database="CREATE DATABASE openclimbing OWNER openclimbing;"
    pg_command="$pg_role $pg_database"
    pg_check=$(sudo su --command "psql -l | grep openclimbing" - postgres)

    if [ -z $pg_check ] ; then
        echo "** Postgres setup. **"
        sudo su --command="psql -f /vagrant/setup/pg_setup.sql" - postgres
        #sudo su --command="psql -c \"CREATE ROLE openclimbing PASSWORD 'climb' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;\"" - postgres
        #sudo su --command="psql -c \"CREATE DATABASE openclimbing OWNER openclimbing;"" - postgres
        #sudo su --command="psql -c \" $pg_role\"" - postgres
        #sudo su --command="psql -c \"$pg_database\"" - postgres
    else
        echo "** Postgres already setup. **"
    fi     # $String is null.


        #sudo su --command="psql -c \"CREATE ROLE openclimbing PASSWORD 'climb' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;\"" - postgres
        #sudo su --command="psql -c \"CREATE DATABASE openclimbing OWNER openclimbing;"" - postgres


    #CREATE ROLE openclimbing PASSWORD 'climb' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;
    #CREATE DATABASE openclimbing OWNER openclimbing;


    # Web Server Setup
    sudo wget -qO- https://toolbelt.heroku.com/install-ubuntu.sh | sh
    sudo foreman export upstart -a openclimbing /etc/init -u root -d /vagrant
    sudo restart openclimbing
  SHELL
end
