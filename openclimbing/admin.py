from django.contrib import admin
from openclimbing import models

admin.site.register(models.Crag)
admin.site.register(models.Location)
admin.site.register(models.ProtectionType)
admin.site.register(models.Route)
admin.site.register(models.Grade)
