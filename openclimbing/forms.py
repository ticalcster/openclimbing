from django import forms
from django.contrib import auth


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(widget=forms.PasswordInput(), required=True)

    _user = None

    def is_valid(self):
        _is_valid = super(LoginForm, self)
        if _is_valid:
            self.full_clean()
            username = self.cleaned_data['username']
            password = self.cleaned_data['password']
            self._user = auth.authenticate(username=username, password=password)
            if self._user is None:
                self.add_error(None, u'Login failed.')
                return False
        return _is_valid

    def login(self, request):
        if self._user is not None:
            if self._user.is_active:
                auth.login(request, self._user)
                return True
        self.add_error(None, u'Login disabled.')
        return False


class RegisterForm(forms.Form):
    email = forms.EmailField(max_length=100)
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(widget=forms.PasswordInput(), required=True)
    password_check = forms.CharField(widget=forms.PasswordInput(), required=True)


class ForgotLoginForm(forms.Form):
    email = forms.EmailField(max_length=100)