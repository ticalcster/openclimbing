from django.contrib import auth
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.decorators.cache import cache_page
from django.views.generic import View, TemplateView, edit
from openclimbing import forms


def index(request):
    return render(request, 'climb/index.html', {})


def status(request):
    return render(request, 'climb/status.html', {})


def todo(request):
    return render(request, 'climb/todo.html', {})


class LoginView(edit.FormView):
    """Login View for authenticating users."""
    template_name = 'climb/auth_login.html'
    form_class = forms.LoginForm
    success_url = reverse_lazy('index')  # TODO send to where they wanted to go

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        # TODO move this in the form
        if form.login(self.request):
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)


def auth_logout(request):
    auth.logout(request)
    return render(request, 'climb/auth_logout.html', {})


class RegisterView(edit.FormView):
    """Register new users to the site."""
    template_name = 'climb/auth_register.html'
    form_class = forms.RegisterForm
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        # TODO create new user!
        return super(RegisterView, self).form_valid(form)


def robots_txt(request):
    return render(request, 'robots.txt', {}, content_type='text/plain')