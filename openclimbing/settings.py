"""
Django settings for openclimbing project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import dj_database_url
import dj_redis_url

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
DEFAULT_SECRET_KEY = 'xnl%!n&4$v*j9pje*mo8d57lk17!=+=c8j)38$rzmszs8n^^i8'
SECRET_KEY = os.environ.get('SECRET_KEY', DEFAULT_SECRET_KEY)

# SECURITY WARNING: don't run with debug turned on in production!
DEFAULT_DEBUG = False
DEBUG = os.environ.get('DEBUG', DEFAULT_DEBUG)

DEFAULT_TEMPLATE_DEBUG = False
#TEMPLATE_DEBUG = os.environ.get('TEMPLATE_DEBUG', DEFAULT_TEMPLATE_DEBUG)

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'openclimbing',
    'ws4redis',
    'bootstrap3',
    "compressor",
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    'django.core.context_processors.request',
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "ws4redis.context_processors.default",
 )

ROOT_URLCONF = 'openclimbing.urls'

WSGI_APPLICATION = 'ws4redis.django_runserver.application'
#WSGI_APPLICATION = 'openclimbing.wsgi.application'

REDIS_DEFAULT = ''
REDIS = dj_redis_url.parse(os.environ.get('REDISCLOUD_URL', REDIS_DEFAULT))

SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_HOST = REDIS['HOST']
SESSION_REDIS_PORT = REDIS['PORT']
SESSION_REDIS_DB = REDIS['DB']
SESSION_REDIS_PASSWORD = REDIS['PASSWORD']
SESSION_REDIS_PREFIX = 'session'

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': '%s:%s' % (REDIS['HOST'], REDIS['PORT']),
        'KEY_PREFIX': 'cache',
        'OPTIONS': {
            'DB': REDIS['DB'],
            'PASSWORD': REDIS['PASSWORD'],
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 100,
                'timeout': 20,
            }
        },
    },
}


WEBSOCKET_URL = '/ws/'

WS4REDIS_HEARTBEAT = '--heartbeat--'
WS4REDIS_PREFIX = 'ws'
WS4REDIS_CONNECTION = {
    'host': REDIS['HOST'],
    'port': REDIS['PORT'],
    'db': REDIS['DB'],
    'password': REDIS['PASSWORD'],
}

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
# Parse database configuration from $DATABASE_URL
DATABASES['default'] = dj_database_url.config()


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static asset configuration
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

COMPRESS_PRECOMPILERS = (
#    ('text/coffeescript', 'coffee --compile --stdio'),
    ('text/less', 'lesscpy {infile}'),
#    ('text/x-sass', 'sass {infile} {outfile}'),
#    ('text/x-scss', 'sass --scss {infile} {outfile}'),
#    ('text/stylus', 'stylus < {infile} > {outfile}'),
#    ('text/foobar', 'path.to.MyPrecompilerFilter'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)