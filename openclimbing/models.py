from django.db import models


class Location(models.Model):
    parent_location = models.ForeignKey('self', blank=True, null=True, db_index=False) #TODO we want an index

    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True)

    def __unicode__(self):              # __str__ on Python 3
        return self.name


class Crag(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True)

    def __unicode__(self):              # __str__ on Python 3
        return self.name

ROUTE_TYPE = (
    ('boulder', 'Boulder'),
    ('top_rope', 'Top Rope'),
    ('sport', 'Sport'),
    ('traditional', 'Trad'),
)


class Grade(models.Model):
    type = models.CharField(max_length=20, choices=ROUTE_TYPE)
    grade = models.CharField(max_length=10)
    value = models.DecimalField(max_digits=6, decimal_places=4)

    def __unicode__(self):              # __str__ on Python 3
        return self.grade


class Route(models.Model):
    crag = models.ForeignKey(Crag)
    grade = models.ForeignKey(Grade)

    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True)

    def __unicode__(self):              # __str__ on Python 3
        return self.name


class ProtectionType(models.Model):
    name = models.CharField(max_length=100)

    def __unicode__(self):              # __str__ on Python 3
        return self.name
