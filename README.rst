Open Climbing
=============

from '/vagrant'

    foreman run python ./manage.py createsuperuser

watch for changes

    $ sudo bash /vagrant/watch.sh /vagrant/openclimbing/ restart openclimbing

or use foreman

    /vagrant$ foreman run python ./manage.py runserver 0.0.0.0:5000

.. code:: python

  def my_function():
      "just a test"
      print 8/2